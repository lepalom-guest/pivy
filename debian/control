Source: pivy
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Teemu Ikonen <tpikonen@gmail.com>, Kurt Kremitzki <kurt@kwk.systems>
Section: python
Priority: optional
Build-Depends: debhelper (>= 11),
               dh-python,
               python-dbg,
               python-dev,
               python-numpy,
               python-pyside2.qtcore,
               python-pyside2.qtgui,
               python-pyside2.qtopengl,
               python-pyside2.qtwidgets,
               python-setuptools,
               python3-dbg,
               python3-dev,
               python3-numpy,
               python3-pyside2.qtcore,
               python3-pyside2.qtgui,
               python3-pyside2.qtopengl,
               python3-pyside2.qtwidgets,
               python3-setuptools,
               libsoqt520-dev,
               libcoin-dev,
               libsimage-dev,
               swig,
	       cmake,
	       qt5-default
Standards-Version: 4.2.0
Vcs-Browser: https://salsa.debian.org/science-team/pivy
Vcs-Git: https://salsa.debian.org/science-team/pivy.git
Homepage: https://bitbucket.org/Coin3D/pivy

Package: python-pivy
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python:Depends},
         python-pyside2.qtcore,
         python-pyside2.qtgui,
         python-pyside2.qtopengl,
         python-pyside2.qtwidgets,
         shiboken2
Provides: ${python:Provides}
Description: Coin binding for Python
 Pivy is a Coin binding for Python. Coin is a high-level 3D graphics library
 with a C++ API. Coin uses scene-graph data structures to render real-time
 graphics suitable for mostly all kinds of scientific and engineering
 visualization applications.
 Pivy allows:
 .
    * Development of Coin applications and extensions in Python
    * Interactive modification of Coin programs from within the
      Python interpreter at runtime
    * Incorporation of Scripting Nodes into the scene graph which
      are capable of executing Python code and callbacks
    * Use of Coin within PyQt4 applications with the quarter module

Package: python3-pivy
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         python3-pyside2.qtcore,
         python3-pyside2.qtgui,
         python3-pyside2.qtopengl,
         python3-pyside2.qtwidgets,
         shiboken2
Provides: ${python3:Provides}
Description: Coin binding for Python 3
 Pivy is a Coin binding for Python. Coin is a high-level 3D graphics library
 with a C++ API. Coin uses scene-graph data structures to render real-time
 graphics suitable for mostly all kinds of scientific and engineering
 visualization applications.
 Pivy allows:
 .
    * Development of Coin applications and extensions in Python
    * Interactive modification of Coin programs from within the
      Python interpreter at runtime
    * Incorporation of Scripting Nodes into the scene graph which
      are capable of executing Python code and callbacks
    * Use of Coin within PyQt4 applications with the quarter module
 .
 This package contains the Python 3 bindings.
